package co.antiqu.chunkgangs.util.database;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.objects.ChunkPlayer;
import co.antiqu.chunkgangs.objects.gangs.Gang;
import co.antiqu.chunkgangs.util.database.mongodb.MongoDatabaseNotEnabled;
import co.antiqu.chunkgangs.util.database.mongodb.MongoManager;
import com.mongodb.client.MongoCollection;
import org.bson.BSON;
import org.bson.conversions.Bson;
import org.bson.Document;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MongoPlayerManager {

    private ChunkGangs instance;

    private MongoManager mongoManager;

    private MongoCollection collection;

    public MongoPlayerManager(ChunkGangs instance) {
        this.instance = instance;
        init();
    }

    private void init() {
                Logger mongoLogger = Logger.getLogger( "org.mongodb.driver" );
                mongoLogger.setLevel(Level.SEVERE);
                try {
                    mongoManager = new MongoManager(instance.getConfig(), instance.getConfig().getString("MongoDB.database"));
                    collection = mongoManager.getCollection("Gangs");
                } catch (MongoDatabaseNotEnabled e) {
                    e.printStackTrace();
                    help();
                }
    }


    public HashMap<UUID, ChunkPlayer> loadPlayers() {
        HashMap<UUID, ChunkPlayer> chunkPlayers = new HashMap<>();
        for(Object n : collection.find()) {
            Document document = (Document) n;
            UUID uuid = UUID.fromString(document.getString("UUID"));
            String gang = document.getString("Gang");
            chunkPlayers.put(uuid, new ChunkPlayer(uuid.toString(),gang,instance));
        }
        return chunkPlayers;
    }


    /*
    new BukkitRunnable() {
            @Override
            public void run() {



            }
        }.runTaskAsynchronously(instance);
    */

    public boolean hasPlayer(Player player) {
        return collection.find(new Document("UUID", player.getUniqueId().toString())).first() != null;
    }



    public void addPlayer(ChunkPlayer chunkPlayer) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if(collection.find(new Document("UUID", chunkPlayer.getUuid().toString())).first() != null)
                    return;
                Document document = new Document("UUID", chunkPlayer.getUuid().toString());
                document.append("Gang", chunkPlayer.getGang().name());
                collection.insertOne(document);
            }
        }.runTaskAsynchronously(instance);

    }

    public void addPlayer(ChunkPlayer chunkPlayer, Gang gang) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if(collection.find(new Document("UUID", chunkPlayer.getUuid().toString())).first() != null)
                    return;
                Document document = new Document("UUID", chunkPlayer.getUuid().toString());
                document.append("Gang", gang.name());
                collection.insertOne(document);
            }
        }.runTaskAsynchronously(instance);
    }


    public void updateGang(ChunkPlayer chunkPlayer, Gang gang) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if(collection.find(new Document("UUID", chunkPlayer.getUuid().toString())).first() == null) {
                    addPlayer(chunkPlayer,gang);
                    return;
                }
                collection.updateOne((Document) Objects.requireNonNull(
                        collection.find(new Document("UUID", chunkPlayer.getUuid().toString())).first()),
                        new Document("$set",
                                new Document("Gang", gang.name())));
            }
        }.runTaskAsynchronously(instance);
      }

    public Gang getGang(ChunkPlayer chunkPlayer) {
        final String[] gang = {""};
        new BukkitRunnable() {
            @Override
            public void run() {
                if(collection.find(new Document("UUID", chunkPlayer.getUuid().toString())).first() == null) {
                    gang[0] = Gang.NONE.name();
                    return;
                }
                gang[0] =  ((Document) Objects.requireNonNull(
                        collection.find(new Document("UUID", chunkPlayer.getUuid().toString())).first())).getString("Gang");
            }
        }.runTaskAsynchronously(instance);
        return instance.getGangManager().getGangFromString(gang[0]);
    }

    private void help() {
        System.out.println("\nMongo DB Not Enabled for ChunkRankup\n");
        instance.getPluginLoader().disablePlugin(instance);
    }
}
