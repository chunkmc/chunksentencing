package co.antiqu.chunkgangs.util.database.mongodb;

public interface MongoClass {

    /**
     * Interface for MongoManager
     * Used to Make MongoInstances
     */

    /**
     *
     * @param db
     * @throws MongoDatabaseNotEnabled
     */
    public abstract void connect(String db) throws MongoDatabaseNotEnabled, MongoDatabaseNotEnabled;

    /**
     *
     * @param ob
     * @param st
     * @throws MongoDatabaseNotEnabled
     */
    public abstract void isNull(Object ob, String st) throws MongoDatabaseNotEnabled, MongoDatabaseNotEnabled;
}
