package co.antiqu.chunkgangs.util.database.mongodb;

import lombok.Getter;

public class MongoDatabaseNotEnabled extends Exception {

    @Getter
    private boolean disabled = false;

    /**
     * Default Constructor
     */

    public MongoDatabaseNotEnabled() {
        super("Unknown Error, check stacktrace.");
    }

    /**
     *
     * @param exception
     */

    public MongoDatabaseNotEnabled(String exception) {
        super(exception);
    }

    /**
     *
     * @param disabled
     */

    public MongoDatabaseNotEnabled(boolean disabled) {
        super("Disabled");
        this.disabled = disabled;
    }

    public MongoDatabaseNotEnabled(NotEnabledReason reason) {
        super(reason.name());
    }

    public enum NotEnabledReason {
      AUTHENTICICATION,
      DATABASE_NOT_FOUND,
      NO_CONFIG_FILE;
    }
}