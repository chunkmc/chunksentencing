package co.antiqu.chunkgangs.util.database.mongodb;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

public class MongoManager implements MongoClass {

    /**
     * MongoDB Manager
     *
     * Config.yml
     * MongoDB:
     *   enabled: true
     *   host: "localhost"
     *   port: "27017"
     *   username: "username"
     *   password: "rUWNfeE2joPLItDJ"
     *
     *  mongodb+srv://[username:password@]host[/[database][?options]]
     *
     */

    @Getter
    public static MongoManager instance;

    @Getter
    private MongoDatabase mongoDatabase;

    @Getter
    private FileConfiguration config;

    @Getter
    private MongoClient mongoClient;

    @Getter
    private boolean enabled;

    private String databaseName;

    /**
     * AUTH CONSTRUCTOR
     * @throws MongoDatabaseNotEnabled
     */
    public MongoManager() throws MongoDatabaseNotEnabled {
        instance = this;
        this.config = null;
        this.databaseName = "Prestige";
        //Collection PrestigePlayers
        connect(databaseName);
    }

    public MongoManager(FileConfiguration config, String databaseName) throws MongoDatabaseNotEnabled {
        instance = this;
        this.config = config;
        this.databaseName = databaseName;
        if(!config.getBoolean("MongoDB.enabled")) {
            enabled = false;
            throw new MongoDatabaseNotEnabled(true);
        }
        connect(databaseName);
    }

    /**
     * AUTH CONNECTION
     * @throws MongoDatabaseNotEnabled
     */

    public void connect() throws MongoDatabaseNotEnabled {
        String username = "intake";
        String password = "LCAm3kQGb5SbcomgLCAm3kQGb5Sbcomg";
        isNull(new Object[] {username,password},"Incorrect Config Pulling");

        String url = "mongodb+srv://"+username+":"+password+"@plugin-gr5vb.mongodb.net/test";

        //this.mongoClient = MongoClients.create(url);
        isNull(mongoClient,MongoDatabaseNotEnabled.NotEnabledReason.AUTHENTICICATION);

        this.mongoDatabase = mongoClient.getDatabase(databaseName);
        isNull(this.mongoDatabase, MongoDatabaseNotEnabled.NotEnabledReason.DATABASE_NOT_FOUND);
    }

    public void connect(String databaseName) throws MongoDatabaseNotEnabled {

        String username = config.getString("MongoDB.username");
        String password = config.getString("MongoDB.password");
        String host = config.getString("MongoDB.host");
        int port = config.getInt("MongoDB.port");

        /*
        if((!host.equalsIgnoreCase("localhost") && port != 27017) || (!username.equalsIgnoreCase("null") && !password.equalsIgnoreCase("null"))) {
            isNull(new Object[] {username,password,host,port},MongoDatabaseNotEnabled.NotEnabledReason.NO_CONFIG_FILE);

            String url = "mongodb+srv://"+username+":"+password+"@"+host+":"+port;

            MongoClient mongoClient = new MongoClient(host, port);
            isNull(mongoClient,MongoDatabaseNotEnabled.NotEnabledReason.AUTHENTICICATION);

            this.mongoDatabase = mongoClient.getDatabase(databaseName);
            isNull(this.mongoDatabase, MongoDatabaseNotEnabled.NotEnabledReason.DATABASE_NOT_FOUND);
            return;
        }*/

        //isNull(new Object[] {username,password,host,port},MongoDatabaseNotEnabled.NotEnabledReason.NO_CONFIG_FILE);

        if(host.equalsIgnoreCase("localhost") && port == 27017) {
            com.mongodb.MongoClient mongoClient = new com.mongodb.MongoClient(host,port);
            isNull(mongoClient,MongoDatabaseNotEnabled.NotEnabledReason.AUTHENTICICATION);

            this.mongoDatabase = mongoClient.getDatabase(databaseName);
            isNull(this.mongoDatabase, MongoDatabaseNotEnabled.NotEnabledReason.DATABASE_NOT_FOUND);
            return;
        }

        String url = "mongodb+srv://"+username+":"+password+"@"+host+":"+port;

        MongoClient mongoClient = MongoClients.create(url);
        isNull(mongoClient,MongoDatabaseNotEnabled.NotEnabledReason.AUTHENTICICATION);

        this.mongoDatabase = mongoClient.getDatabase(databaseName);
        isNull(this.mongoDatabase, MongoDatabaseNotEnabled.NotEnabledReason.DATABASE_NOT_FOUND);
    }

    private FileConfiguration getConfig() {
        return config;
    }

    public MongoCollection getCollection(String collectionName) {
        if(!mongoDatabase.listCollectionNames().into(new ArrayList<String>()).contains(collectionName))
            mongoDatabase.createCollection(collectionName);
        return mongoDatabase.getCollection(collectionName);
    }

    public void isNull(Object object, String error) throws MongoDatabaseNotEnabled {
        if(object == null) {
            throw new MongoDatabaseNotEnabled(error);
        }
    }

    public void isNull(Object object, MongoDatabaseNotEnabled.NotEnabledReason error) throws MongoDatabaseNotEnabled {
        if(object == null) {
            throw new MongoDatabaseNotEnabled(error);
        }
    }

    public void isNull(List<Object> object, String error) throws MongoDatabaseNotEnabled {
        for (Object o : object) {
            if (o == null) {
                throw new MongoDatabaseNotEnabled(error);
            }
        }
    }
}
