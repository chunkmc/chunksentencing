package co.antiqu.chunkgangs.util;

import lombok.Getter;

public enum Permission {

    NONE(""),
    STAFF("chunkgangs.staff");

    @Getter
    private String perm;

    Permission(String s) {
        this.perm = s;
    }

}
