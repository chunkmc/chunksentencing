package co.antiqu.chunkgangs.util;

import co.antiqu.chunkgangs.ChunkGangs;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

public class Lang {

    private ChunkGangs instance;

    private static FileConfiguration config = ChunkGangs.getInstance().getLang().getConfig();

    public static String NO_PERMISSION;
    public static String INCORRECT_USAGE;

    public static String MURDER_TAG;
    public static String ROBBER_TAG;
    public static String DEALER_TAG;

    public static String PICKPOCKET_ITEM_NAME;
    public static int PICKPOCKER_TIMER;
    public static String PICKPOCKET_MESSAGE_PLAYER;

    public static String KILED_PLAYER_AMOUNT;

    public static String BLEEDING;

    public static String SHIV_NAME;

    public Lang(ChunkGangs instance) {
        this.instance = instance;
        NO_PERMISSION = Color.t(config.getString("no_permission"));
        INCORRECT_USAGE = Color.t(config.getString("incorrect_usage"));
        MURDER_TAG = Color.t(instance.getConfig().getString("tags.murder"));
        ROBBER_TAG = Color.t(instance.getConfig().getString("tags.robber"));
        DEALER_TAG = Color.t(instance.getConfig().getString("tags.dealer"));
        PICKPOCKET_ITEM_NAME = Color.t(instance.getConfig().getString("customitems.pickpocket"));
        PICKPOCKER_TIMER = instance.getConfig().getInt("customitems.pickpocket_timer");
        PICKPOCKET_MESSAGE_PLAYER = Color.t(config.getString("pickpocketed"));
        KILED_PLAYER_AMOUNT = Color.t(config.getString("killed_player_buff"));
        BLEEDING = Color.t(config.getString("bleeding"));
        SHIV_NAME = Color.t(instance.getConfig().getString("customitems.shiv"));
    }
}
