package co.antiqu.chunkgangs.integrations;

import co.antiqu.chunkgangs.ChunkGangs;
import lombok.Getter;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.plugin.RegisteredServiceProvider;

public class IVault implements Integration {

    @Getter
    private IVault inegration;

    @Getter
    private ChunkGangs instance;

    @Getter
    private Economy economy;

    @Getter
    private Chat chat;

    @Getter
    private Permission permission;

    public IVault(ChunkGangs instance) {
        inegration = this;
        this.instance = instance;
        setupEconomy();
        setupPermissions();
        setupChat();
    }

    private boolean setupEconomy() {
        if (instance.getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = instance.getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        economy = rsp.getProvider();
        return economy != null;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = instance.getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = instance.getServer().getServicesManager().getRegistration(Permission.class);
        permission = rsp.getProvider();
        return permission != null;
    }


    public boolean isValid() {
        if (instance.getServer().getPluginManager().getPlugin("Vault") == null || !instance.getServer().getPluginManager().getPlugin("Vault").isEnabled()) {
            instance.getPluginLoader().disablePlugin(instance);
            return false;
     }
        return true;
    }
}
