package co.antiqu.chunkgangs.integrations;

import co.antiqu.chunkgangs.ChunkGangs;
import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.clip.placeholderapi.external.EZPlaceholderHook;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class IPlaceHolderAPI extends PlaceholderExpansion implements Integration {

    private ChunkGangs instance;

    public IPlaceHolderAPI(ChunkGangs instance) {
        this.instance = instance;
        isValid();
        init();
    }

    private void init() {
        PlaceholderAPI.registerExpansion(this);
    }

    //%chunkgangs_gang%
    //%chunkgangs_gang_tag%

    @Override
    public String onPlaceholderRequest(Player p, String params) {
        if(p == null || !p.isOnline())
            return "";
        if(params.equalsIgnoreCase("gang"))
            return instance.getChunkPlayerManager().getChunkPlayer(p).getGang().name().toLowerCase();
        if(params.equalsIgnoreCase("gang_tag"))
            return instance.getChunkPlayerManager().getChunkPlayer(p).getGangTag();
        return null;
    }

    @Override
    public boolean isValid() {
        if (instance.getServer().getPluginManager().getPlugin("PlaceholderAPI") == null) {
            instance.getPluginLoader().disablePlugin(instance);
            return false;
        }
        return true;
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getIdentifier() {
        return "chunkgangs";
    }

    @Override
    public String getAuthor() {
        return "AlexElg";
    }

    @Override
    public String getVersion() {
        return "1.0.1";
    }
}
