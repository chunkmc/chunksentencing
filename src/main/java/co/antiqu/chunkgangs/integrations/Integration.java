package co.antiqu.chunkgangs.integrations;

public interface Integration {

    public abstract boolean isValid();

}
