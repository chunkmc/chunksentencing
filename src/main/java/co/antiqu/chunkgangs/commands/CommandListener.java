package co.antiqu.chunkgangs.commands;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.util.Color;
import co.antiqu.chunkgangs.util.Permission;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CommandListener implements CommandExecutor, TabCompleter {

    protected ChunkGangs instance;

    public CommandListener(ChunkGangs instance) {
        this.instance = instance;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {

        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        if(strings.length == 1) {
            if(commandSender.hasPermission(Permission.STAFF.getPerm()))
                return Color.t(Arrays.asList("bal"));
            return Color.t(Arrays.asList("bal"));
        }
        return null;
    }
}
