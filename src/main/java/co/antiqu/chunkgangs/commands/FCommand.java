package co.antiqu.chunkgangs.commands;

import co.antiqu.chunkgangs.util.Permission;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public abstract class FCommand {

    @Getter
    @Setter
    private String command;
    @Getter @Setter
    private Permission permission;
    @Getter @Setter
    private String usage;

    @Getter @Setter
    private boolean console;
    @Getter @Setter
    private boolean player;

    @Getter @Setter
    private Player playerSender;

    @Getter
    private Set<String> args;

    public void addArg(String arg) {
        args.add(arg);
    }

    public abstract void run(CommandSender sender, String[] args);

    public FCommand() {
        this.args = new HashSet<>();
    }

}