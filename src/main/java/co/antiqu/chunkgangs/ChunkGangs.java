package co.antiqu.chunkgangs;

import co.antiqu.chunkgangs.commands.CommandListener;
import co.antiqu.chunkgangs.integrations.IPlaceHolderAPI;
import co.antiqu.chunkgangs.integrations.IVault;
import co.antiqu.chunkgangs.objects.ChunkPlayerManager;
import co.antiqu.chunkgangs.objects.buffs.BuffManager;
import co.antiqu.chunkgangs.objects.buffs.PickPocket;
import co.antiqu.chunkgangs.objects.gangs.GangManager;
import co.antiqu.chunkgangs.objects.stories.StoriesManager;
import co.antiqu.chunkgangs.util.Config;
import co.antiqu.chunkgangs.util.Lang;
import co.antiqu.chunkgangs.util.database.MongoPlayerManager;
import lombok.Getter;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class ChunkGangs extends JavaPlugin {

    @Getter
    private static ChunkGangs instance;

    @Getter
    private IVault iVault;

    @Getter
    private IPlaceHolderAPI placeHolderAPI;

    @Getter
    private Config lang;

    @Getter
    private Config stories;

    @Getter
    private MongoPlayerManager mongoPlayerManager;

    @Getter
    private CommandListener commandListener;

    @Getter
    private Lang langClass;

    @Getter
    private GangManager gangManager;

    @Getter
    private ChunkPlayerManager chunkPlayerManager;

    @Getter
    private StoriesManager storiesManager;

    @Getter
    private BuffManager buffManager;

    @Override
    public void onEnable() {
        instance = this;
        setUp();
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    private void setUp() {
        config();
        initegrations();
        objects();
        listeners();
        commands();
    }

    private void initegrations() {
        this.iVault = new IVault(instance);
        iVault.isValid();
        this.placeHolderAPI = new IPlaceHolderAPI(instance);
        placeHolderAPI.isValid();
    }

    public void config() {
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
        this.lang = new Config(instance,"lang.yml");
        this.lang.saveDefaultConfig(instance);
        mongoPlayerManager = new MongoPlayerManager(instance);
        this.stories = new Config(instance,"stories.yml");
        this.stories.saveDefaultConfig(instance);
        this.buffManager = new BuffManager(instance);
        Bukkit.getPluginManager().registerEvents(buffManager,instance);
    }

    private void listeners() {

    }

    private void commands() {
        this.commandListener = new CommandListener(instance);
    }

    private void objects() {
        this.langClass = new Lang(instance);
        this.storiesManager = new StoriesManager(instance);
        Bukkit.getPluginManager().registerEvents(storiesManager,instance);
        this.gangManager = new GangManager(instance);
        this.chunkPlayerManager = new ChunkPlayerManager(instance);
        Bukkit.getPluginManager().registerEvents(chunkPlayerManager,instance);
    }

}
