package co.antiqu.chunkgangs.objects.stories;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.objects.ChunkPlayer;
import co.antiqu.chunkgangs.objects.gangs.Gang;
import lombok.var;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class StoriesManager implements Listener {

    protected ChunkGangs instance;

    private FileConfiguration config;
    private int interval;
    private HashMap<Gang,Story> stories;

    public Set<UUID> onStory;

    public StoriesManager(ChunkGangs instance) {
        this.instance = instance;
        init();
    }

    private void init() {
        this.config = instance.getStories().getConfig();
        stories = new HashMap<>();
        onStory = new HashSet<>();
        try {
            this.interval = config.getInt("stories.interval");
            stories.put(Gang.DEALER,new Story(config.getStringList("stories.dealer")));
            stories.put(Gang.MURDER,new Story(config.getStringList("stories.murder")));
            stories.put(Gang.ROBBER,new Story(config.getStringList("stories.robber")));
        } catch (Exception e) {
            System.out.println("No stories or interval loaded, or a missing story, refer to default stories.yml");
            e.printStackTrace();
        }
    }

    public void sendMessage(ChunkPlayer chunkPlayer) {
        if(stories.keySet().contains(chunkPlayer.getGang())) {
            Iterator<String> story = stories.get(chunkPlayer.getGang()).getStory().iterator();
            if(stories.get(chunkPlayer.getGang()).getStory().isEmpty())
                return;
            if(chunkPlayer.getOfflinePlayer().getPlayer() == null || !chunkPlayer.getOfflinePlayer().getPlayer().isOnline())
                return;
            onStory.add(chunkPlayer.getUuid());
            new BukkitRunnable() {
                @Override
                public void run() {
                    if(!story.hasNext()) {
                        onStory.remove(chunkPlayer.getUuid());
                        cancel();
                        return;
                    }
                    String line = story.next();
                    chunkPlayer.getOfflinePlayer().getPlayer()
                            .sendMessage(line.replaceAll("%player%", chunkPlayer.getOfflinePlayer().getName()));
                }
            }.runTaskTimer(instance,10,interval*20);
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent evt) {
        if(onStory.contains(evt.getPlayer().getUniqueId()))
            evt.setCancelled(true);
        if(onStory.isEmpty())
            return;
        onStory.forEach(n -> {
            Player player = Bukkit.getPlayer(n);
            if(player == null || !player.isOnline())
                return;
            evt.getRecipients().remove(player);
        });
    }

}
