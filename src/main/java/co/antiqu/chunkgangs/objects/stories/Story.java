package co.antiqu.chunkgangs.objects.stories;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.util.Color;
import lombok.Getter;

import java.util.List;

public class Story {

    protected ChunkGangs instance;

    @Getter
    private List<String> story;

    public Story(List<String> story) {
        this.story = Color.t(story);
    }

}
