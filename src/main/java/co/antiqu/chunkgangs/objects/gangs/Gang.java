package co.antiqu.chunkgangs.objects.gangs;

import lombok.Getter;

public enum Gang {
    NONE,
    MURDER,
    ROBBER,
    DEALER;
}
