package co.antiqu.chunkgangs.objects.gangs;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.objects.ChunkPlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class GangManager {

    protected ChunkGangs instance;
    private Gang[] gangs;
    private HashMap<Gang, List<String>> gangPermissions;
    private FileConfiguration config;

    public GangManager(ChunkGangs instance) {
        this.instance = instance;
        init();
    }

    private void init() {
        this.config = instance.getConfig();
        gangs = new Gang[4];
        gangs[1] = Gang.DEALER;
        gangs[2] = Gang.MURDER;
        gangs[3] = Gang.ROBBER;
        gangPermissions = new HashMap<>();
        try {
            gangPermissions.put(Gang.ROBBER, config.getStringList("permissions.robber"));
            gangPermissions.put(Gang.DEALER, config.getStringList("permissions.dealer"));
            gangPermissions.put(Gang.MURDER, config.getStringList("permissions.murder"));

        } catch (Exception e) {
            System.out.println("No gang permissions loaded, refer to default config.yml");
            e.printStackTrace();
        }
    }

    public Gang getGangFromString(String s) {
        switch (s.toUpperCase()) {
            case "DEALER": return Gang.DEALER;
            case "MURDER": return Gang.MURDER;
            case "ROBBER": return Gang.ROBBER;
        }
        return null;
    }

    public void addPermission(ChunkPlayer chunkPlayer) {
        Player player = chunkPlayer.getOfflinePlayer().getPlayer();
        if(player == null || !player.isOnline())
            return;
        if(gangPermissions.containsKey(chunkPlayer.getGang())) {
            gangPermissions.get(chunkPlayer.getGang()).forEach(n -> ChunkGangs.getInstance().getIVault().getPermission().playerAdd(player,n));
        }
    }

    public Gang getRandomGang() {
        return gangs[ThreadLocalRandom.current().nextInt(0,4)];
    }

}
