package co.antiqu.chunkgangs.objects;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.objects.gangs.Gang;
import co.antiqu.chunkgangs.util.Lang;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.UUID;

public class ChunkPlayer {

    protected ChunkGangs instance;

    @Getter
    private UUID uuid;

    @Getter
    private OfflinePlayer offlinePlayer;

    @Getter
    private Gang gang;

    @Getter
    private String gangTag;

    public ChunkPlayer(String uuidi, String gang, ChunkGangs instance) {
        this.instance = instance;
        this.uuid = UUID.fromString(uuidi);
        this.offlinePlayer = Bukkit.getOfflinePlayer(uuid);

        if(gang.equalsIgnoreCase(Gang.NONE.name())) {
            this.gang = instance.getGangManager().getRandomGang();
        } else {
            this.gang = instance.getGangManager().getGangFromString(gang);
        }

        switch(getGang()) {
            case ROBBER: {
                this.gangTag = Lang.ROBBER_TAG;
            } break;
            case DEALER: {
                this.gangTag = Lang.DEALER_TAG;
            } break;
            case MURDER: {
                this.gangTag = Lang.MURDER_TAG;
            } break;
        }

    }

}
