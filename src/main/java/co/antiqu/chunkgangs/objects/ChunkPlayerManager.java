package co.antiqu.chunkgangs.objects;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.objects.gangs.Gang;
import co.antiqu.chunkgangs.util.Color;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashMap;
import java.util.UUID;

public class ChunkPlayerManager implements Listener {

    protected ChunkGangs instance;

    private HashMap<UUID,ChunkPlayer> chunksPlayers;

    public ChunkPlayerManager(ChunkGangs instance) {
        this.instance = instance;
        init();
    }

    private void init() {
        chunksPlayers = instance.getMongoPlayerManager().loadPlayers();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent evt) {
        if(chunksPlayers.keySet().contains(evt.getPlayer().getUniqueId()))
            return;
        ChunkPlayer chunkPlayer = new ChunkPlayer(evt.getPlayer().getUniqueId().toString(), Gang.NONE.name(),instance);
        chunksPlayers.put(evt.getPlayer().getUniqueId(),chunkPlayer);
        instance.getMongoPlayerManager().addPlayer(chunkPlayer);
        instance.getStoriesManager().sendMessage(chunkPlayer);
        instance.getGangManager().addPermission(chunkPlayer);
    }

    public ChunkPlayer getChunkPlayer(Player player) {
        if(chunksPlayers.containsKey(player.getUniqueId())) {
            return chunksPlayers.get(player.getUniqueId());
        } else {
            player.kickPlayer(Color.t("&cRejoin for a Profile Creation"));
        }
        return null;
    }

}
