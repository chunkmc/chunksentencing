package co.antiqu.chunkgangs.objects.buffs;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.objects.ChunkPlayer;
import co.antiqu.chunkgangs.objects.gangs.Gang;
import co.antiqu.chunkgangs.util.Lang;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class BuffManager implements Listener {

    protected ChunkGangs instance;

    @Getter
    private PickPocket pickPocket;

    @Getter
    private ShivBuff shivBuff;

    @Getter
    private Death2Percent death2Percent;

    public BuffManager(ChunkGangs instance) {
        this.instance = instance;
        init();
    }

    private void init() {
        buffs();
        new BukkitRunnable() {
            @Override
            public void run() {
                for(Player n : Bukkit.getOnlinePlayers()) {
                    ChunkPlayer chunkPlayer = instance.getChunkPlayerManager().getChunkPlayer(n);
                    if(chunkPlayer == null)
                        continue;
                    switch(chunkPlayer.getGang()) {
                        case DEALER: {
                            n.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 1, true, true));
                        } break;

                        case MURDER: {
                            n.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 200, 1, true, true));
                        } break;

                        case ROBBER: {
                            n.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 200, 1, true, true));
                        } break;
                    }
                }
            }
        }.runTaskTimer(instance,0,100);
    }

    private void buffs() {
        pickPocket = new PickPocket(instance);
        Bukkit.getPluginManager().registerEvents(pickPocket, instance);
        shivBuff = new ShivBuff(instance);
        Bukkit.getPluginManager().registerEvents(shivBuff,instance);
        death2Percent = new Death2Percent(instance);
        Bukkit.getPluginManager().registerEvents(death2Percent,instance);
    }
}
