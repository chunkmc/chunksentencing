package co.antiqu.chunkgangs.objects.buffs;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.objects.ChunkPlayer;
import co.antiqu.chunkgangs.objects.gangs.Gang;
import co.antiqu.chunkgangs.util.Lang;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;

public class ShivBuff implements Listener {

    @Getter
    private ChunkGangs instance;

    private HashMap<UUID,Integer> bleedingTimes;

    private Set<PotionEffect> bleedingEffects;

    public ShivBuff(ChunkGangs instance) {
        this.instance = instance;
        init();
    }

    private void init() {
        bleedingTimes = new HashMap<>();
        bleedingEffects = new HashSet<>();
        bleedingEffects.add(new PotionEffect(PotionEffectType.WITHER,200,0));
        bleedingEffects.add(new PotionEffect(PotionEffectType.POISON,200,0));
    }

    /**
     * Shiv Buff
     * Every 10 hits with a shiv, the victim gets Poison and Wither for 10 secconds.
     * @param evt
     */

    @EventHandler
    public void onKill(EntityDamageByEntityEvent evt) {
        if (!(evt.getEntity() instanceof Player) || !(evt.getDamager() instanceof Player))
            return;
        Player player = (Player) evt.getDamager();
        ChunkPlayer chunkPlayer = instance.getChunkPlayerManager().getChunkPlayer(player);
        if (chunkPlayer.getGang() != Gang.MURDER)
            return;
        ItemStack inHand = player.getItemInHand();
        if(inHand == null || inHand.getType() != Material.WOOD_SWORD || !inHand.hasItemMeta() || !inHand.getItemMeta().getDisplayName().equalsIgnoreCase(Lang.SHIV_NAME))
            return;
        updatePlayer(player);
        if(getHits(player) >= 10) {
            removePlayer(player);
            addBleading((Player) evt.getEntity());
        }
    }

    public void addBleading(Player victim) {
        if(!victim.isOnline())
            return;
        bleedingEffects.forEach(victim::addPotionEffect);
    }

    public void removePlayer(OfflinePlayer player) {
        if(!bleedingTimes.containsKey(player.getUniqueId()))
            return;
        bleedingTimes.remove(player.getUniqueId());
    }

    public int getHits(OfflinePlayer player) {
        if(!bleedingTimes.containsKey(player.getUniqueId()))
            return 0;
        return bleedingTimes.get(player.getUniqueId());
    }

    public void updatePlayer(OfflinePlayer player) {
        if(bleedingTimes.containsKey(player.getUniqueId())) {
            bleedingTimes.put(player.getUniqueId(), bleedingTimes.get(player.getUniqueId())+1);
        } else {
            bleedingTimes.put(player.getUniqueId(), 1);
        }
    }



}
