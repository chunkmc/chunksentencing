package co.antiqu.chunkgangs.objects.buffs.runnables;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.util.Lang;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.ThreadLocalRandom;

public class PickPicketRunnable extends BukkitRunnable {

    protected ChunkGangs instance;


    private OfflinePlayer player;
    private OfflinePlayer victim;

    public PickPicketRunnable(OfflinePlayer player, OfflinePlayer victim, ChunkGangs instance) {
        this.instance = instance;
        this.player = player;
        this.victim = victim;
    }

    @Override
    public void run() {
        if(!instance.getBuffManager().getPickPocket().containsPlayer(victim))
            return;
        instance.getBuffManager().getPickPocket().removeCooldownPlayer(victim);
        double amount = instance.getIVault().getEconomy().getBalance(victim) * ThreadLocalRandom.current().nextDouble(0.01,0.03);
        instance.getIVault().getEconomy().withdrawPlayer(victim, amount);
        instance.getIVault().getEconomy().depositPlayer(player,amount);
        if(instance.getIVault().getEconomy().getBalance(victim) <= 0)
            return;
        if(player.getPlayer().isOnline()) {
            player.getPlayer().sendMessage(Lang.PICKPOCKET_MESSAGE_PLAYER.replaceAll("%player%", victim.getName()).replaceAll("%amount%", ""+amount));
        }
    }
}
