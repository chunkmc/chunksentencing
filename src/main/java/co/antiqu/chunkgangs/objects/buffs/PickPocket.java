package co.antiqu.chunkgangs.objects.buffs;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.objects.ChunkPlayer;
import co.antiqu.chunkgangs.objects.buffs.runnables.PickPicketRunnable;
import co.antiqu.chunkgangs.objects.gangs.Gang;
import co.antiqu.chunkgangs.util.Color;
import co.antiqu.chunkgangs.util.Lang;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class PickPocket implements Listener {

    protected ChunkGangs instance;

    private Set<UUID> movePlayers;

    public PickPocket(ChunkGangs instance) {
        this.instance = instance;
        init();
    }

    private void init() {
        movePlayers = new HashSet<>();
    }

    public void removeCooldownPlayer(OfflinePlayer player) {
        movePlayers.remove(player.getUniqueId());
    }

    @EventHandler
    public void onMove(PlayerMoveEvent evt) {
        if(movePlayers.contains(evt.getPlayer().getUniqueId())) {
            removeCooldownPlayer(evt.getPlayer());
        }
    }

    @EventHandler
    public void onDisconet(PlayerQuitEvent evt) {
        if(movePlayers.contains(evt.getPlayer().getUniqueId())) {
            removeCooldownPlayer(evt.getPlayer());
        }
    }

    /**
     * PickPocket Buff (Robber Buff)
     * If a player Right-Clicks on another player with a PickPocket Sword
     * And the victim does not move within x secconds they loose 0.01 - 0.03 their balance
     * which is given to the robber.
     */

    @EventHandler
    public void onInteract(PlayerInteractEntityEvent evt) {
        if(!(evt.getRightClicked() instanceof Player))
            return;
        ChunkPlayer chunkPlayer = instance.getChunkPlayerManager().getChunkPlayer(evt.getPlayer());
        if(chunkPlayer.getGang() != Gang.ROBBER)
            return;
        if(movePlayers.contains(evt.getPlayer()))
            return;
        ItemStack inHand = evt.getPlayer().getInventory().getItemInHand();
        if(inHand == null || !inHand.hasItemMeta() || inHand.getType() != Material.GOLD_SWORD)
            return;
        if(!inHand.getItemMeta().getDisplayName().equalsIgnoreCase(Lang.PICKPOCKET_ITEM_NAME))
            return;
        movePlayers.add(evt.getRightClicked().getUniqueId());
        new PickPicketRunnable(evt.getPlayer(),(Player) evt.getRightClicked(),instance).runTaskLater(instance, Lang.PICKPOCKER_TIMER*20);
    }

    public boolean containsPlayer(OfflinePlayer player) {
        return movePlayers.contains(player.getUniqueId());
    }

}
