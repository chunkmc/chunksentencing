package co.antiqu.chunkgangs.objects.buffs;

import co.antiqu.chunkgangs.ChunkGangs;
import co.antiqu.chunkgangs.objects.ChunkPlayer;
import co.antiqu.chunkgangs.objects.gangs.Gang;
import co.antiqu.chunkgangs.util.Lang;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.concurrent.ThreadLocalRandom;

public class Death2Percent implements Listener {

    @Getter
    private ChunkGangs instance;

    public Death2Percent(ChunkGangs instance) {
        this.instance = instance;
        init();
    }

    private void init() {

    }

    /**
     * Robber Buff
     * 15% Chance to get 2% of Victims Bal on Death
     * @param evt
     */

    @EventHandler
    public void onKill(EntityDamageByEntityEvent evt) {
        if(!(evt.getEntity() instanceof Player) || !(evt.getDamager() instanceof Player))
            return;
        if(!evt.getEntity().isDead())
            return;
        Player player = (Player) evt.getDamager();
        Player victim = (Player) evt.getEntity();
        ChunkPlayer chunkPlayer = instance.getChunkPlayerManager().getChunkPlayer(player);
        if(chunkPlayer.getGang() != Gang.ROBBER)
            return;
        if(ThreadLocalRandom.current().nextInt(0,100) <= 15)
            return;
        if(instance.getIVault().getEconomy().getBalance(victim) <= 0)
            return;
        double amount = instance.getIVault().getEconomy().getBalance(victim) * 0.02;
        instance.getIVault().getEconomy().withdrawPlayer(victim, amount);
        instance.getIVault().getEconomy().depositPlayer(player,amount);
        player.sendMessage(Lang.KILED_PLAYER_AMOUNT.replaceAll("%player%", victim.getName()).replaceAll("%amount%", ""+(int)amount));
    }

}
